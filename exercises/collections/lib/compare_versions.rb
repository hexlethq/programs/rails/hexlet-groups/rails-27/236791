# frozen_string_literal: true

# BEGIN
def compare_versions(first, second)
  a_major, a_minor = first.split('.').map(&:to_i)
  b_major, b_minor = second.split('.').map(&:to_i)
  major_diff = a_major <=> b_major

  return major_diff unless major_diff.zero?

  a_minor <=> b_minor
end
# END
