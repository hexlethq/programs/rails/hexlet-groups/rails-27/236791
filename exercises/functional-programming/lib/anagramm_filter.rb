# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, anagramms)
  anagramms.select { _1.chars.sort == word.chars.sort }
end
# END
