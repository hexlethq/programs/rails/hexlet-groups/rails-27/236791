# frozen_string_literal: true

# BEGIN
def count_by_years(list)
  list.each_with_object(Hash.new(0)) do |el, acc|
    key = el[:birthday].split('-').first
    acc[key] += 1 if el[:gender] == 'male'
  end
end
# END
