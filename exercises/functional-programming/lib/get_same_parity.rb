# frozen_string_literal: true

# BEGIN
def get_same_parity(numbers)
  numbers.filter { _1.odd? == numbers.first.odd? }
end
# END
