# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num <= 0

  first = 0
  second = 1

  (num - 1).times do
    first, second = second, first + second
  end

  first
end
# END
