# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return if start > stop

  i = start
  result = []

  while i <= stop

    tmp = ''
    tmp += 'Fizz' if (i % 3).zero?
    tmp += 'Buzz' if (i % 5).zero?

    result << tmp if tmp.size >= 1
    result << i if tmp.size.zero?

    i += 1
  end

  result.join(' ')
end

# END
