# frozen_string_literal: true

# BEGIN
# hah :)

def reverse(str)
  i = str.size - 1
  result = []
  str.size.times do
    result << str[i]
    i -= 1
  end
  result.join('')
end

# or just for or interpolated string "{current}{prev}"

# END
