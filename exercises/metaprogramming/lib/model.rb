# frozen_string_literal: true

# BEGIN
require 'date'

# add attributes to the class
module Model
  attr_reader :attributes

  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attributes = {})
    set_attributes

    attributes.each do |name, value|
      send("#{name}=", value) if attributes.key?(name)
    end
  end

  # Methods for singleton class methods
  module ClassMethods
    def attribute(name, **options)
      define_attribute(name, options)

      define_method(name) { attributes[name] }

      define_method("#{name}=") do |value|
        attributes[name] = self.class.convert_value(name, value)
      end
    end

    def convert_value(name, value)
      return value if value.nil?

      case schema[name][:type]
      when :string
        value.to_s
      when :datetime
        DateTime.parse(value)
      when :integer
        value.to_i
      when :boolean
        [true, 'true', 1, '1'].include?(value)
      else
        value
      end
    end

    def schema
      @schema ||= {}
    end

    def define_attribute(name, options)
      schema[name] = {
        type: options[:type],
        default: options[:default]
      }
    end
  end

  private

  def set_attributes
    @attributes = self.class.schema.transform_values { |opt| opt[:default] }
  end
end
# END
