# frozen_string_literal: true

# BEGIN
require 'uri'

class Url
  include Comparable

  extend Forwardable
  def_delegators :uri, :scheme, :host, :to_s

  attr_reader :uri

  def initialize(url)
    @uri = URI(url)
  end

  def <=>(other)
    uri.to_s <=> other.to_s
  end

  def query_param(key, default = nil)
    query_params.fetch(key, default)
  end

  def query_params
    return {} unless uri.query

    @query_params ||= uri.query.split('&').map.with_object({}) do |query_param, hash|
      key, value = query_param.split('=')
      hash[key.to_sym] = value
    end
  end
end
# END
