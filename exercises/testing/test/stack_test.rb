# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    elements = [1, 2, 3, 4]
    @stack = Stack.new(elements)
  end

  def test_new
    assert_kind_of(Stack, @stack)
    assert { @stack.to_a == [1, 2, 3, 4] }
  end

  def test_size
    assert { @stack.size == 4 }
  end

  def test_push!
    @stack.push!(5)
    assert { @stack.size == 5 }
    assert { @stack.to_a == [1, 2, 3, 4, 5] }
    refute { @stack.empty? }
  end

  def test_pop!
    poped_el = @stack.pop!

    assert { @stack.size == 3 }
    assert { @stack.to_a == [1, 2, 3] }
    assert { poped_el == 4 }

    refute { @stack.empty? }
  end

  def test_clear!
    @stack.clear!
    assert { @stack.empty? }
    assert { @stack.size.zero? }
    assert { @stack.to_a == [] }
  end

  def test_empty
    stack = Stack.new([])
    assert { stack.empty? }
    assert { stack.to_a == [] }
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
